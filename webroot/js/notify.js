var interval = null;
var lastDate = null;
$(document).ready(function () {

    if (Notify.needsPermission && Notify.isSupported()) {
        doNotification();
    }

    Visibility.change(function (e, state) {
        if (Visibility.hidden()) {
            if (!Notify.needsPermission) {
                doNotification();
            }
        } else {
            if (interval) {
                clearInterval(interval);
            }
        }
    });

});

function onShowNotification() {
    console.log('notification is shown!');
}

function onCloseNotification() {
    console.log('notification is closed!');
}

function onClickNotification() {
    console.log('notification was clicked!');
}

function onErrorNotification() {
    console.error('Error showing notification. You may need to request permission.');
}

function onPermissionGranted() {
    console.log('Permission has been granted by the user');
    doNotification();
}

function onPermissionDenied() {
    console.warn('Permission has been denied by the user');
}

function doNotification() {
    lastDate = new Date().getTime();
    interval = setInterval(function () {
        console.log(lastDate);
        $.post({
            url: BASE + CONTROLLER + "/news",
            data: {date: lastDate}
        }).done(function (data) {
            lastDate = new Date().getTime();
            data = data.data;
            var icon = data.icon ? data.icon : 'https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2015/Oct/24/4145143060-10-xgeo-avatar.png';
            if (data.count > 0) {
                var myNotification = new Notify(data.model, {
                    body: data.count,
                    tag: data.date,
                    icon: icon,
                    notifyShow: onShowNotification,
                    //notifyClose: onCloseNotification,
                    //notifyClick: onClickNotification,
                    //notifyError: onErrorNotification,
                    timeout: 5
                });
                myNotification.show();
            }
        });
    }, 3 * 1000);
}
