angular.module('bookmarkApp').factory('auditAPI', function ($http) {

    var _list = function (page, itemsPerPage) {

        return $http.post(BASE + '/manager/audits/list', {
            page: page,
            per_page: itemsPerPage,
        });
    };

    return {
        list: _list,
    };

});