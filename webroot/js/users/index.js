$(document).ready(function () {
    $(".date-picker").datepicker();
    $(".table").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: BASE + CONTROLLER + "/index",
            dataFilter: function (data) {
                return data;
            },
            "data": function (d) {
                /* Add Filters Here*/
                d.filters = {
                    "interval": [
                        {
                            "field": "created",
                            "type": "date",
                            "start": $("#datestart").val(),
                            "end": $("#dateend").val()
                        },
                        {
                            "field": "bookmarks_count",
                            "type": "int",
                            "start": $("#booksmin").val(),
                            "end": $("#booksmax").val()
                        }
                    ]
                };
                return d;
            }
        },
        "columns": [
            {"data": "id"},
            {"data": "name"},
            {"data": "email"},
            {"data": "created"},
            {"data": "modified"},
            {"data": "bookmarks_count"},
            {"data": "actions"}
        ],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": -1,
            "render": function (data, type, row) {
                return "<a href='users/edit/" + row.id + "'>Edit</a>";
            }
        }]
    });

});



