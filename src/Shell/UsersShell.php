<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 31/03/2016
 * Time: 19:23
 */

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Mailer\Email;

/**
 * Run following command:
 * ./bin/cake users --help
 *
 * Class CronJobUsersShell
 * @package App\Shell
 */
class UsersShell extends Shell
{
    public $tasks = ['SendEmail'];

    public function initialize()
    {
    }

    /**
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addSubcommand('start', [
            'help' => 'Start The The Email Cron Job. (ONY LINUX)'
        ]);
        $parser->addSubcommand('execute', [
            'help' => 'Execute The Email Task Immediately.'
        ]);
        return $parser;
    }

    /**
     *
     */
    public function main()
    {
        $this->out("V " . $this->verify());
    }

    /**
     * @return bool|int
     */
    public function verify()
    {
        if (strpos(strtoupper(PHP_OS), 'LINUX') === true) {
            $this->out('LINUX OPERATIONAL SYSTEM');
        }
        $this->out('OTHER OPERATIONAL SYSTEM');

        return strpos(strtoupper(PHP_OS), 'LINUX');
    }

    /**
     *
     */
    public function start()
    {
        if ($this->verify()) {
            //        $result = shell_exec('1 * * * * cd ' . ROOT . '/bin && cake users execute');
            $result = shell_exec('cd ' . ROOT . '/bin && cake users execute');
            $this->out($result);
            $this->out('cron job started');
        } else {
            $this->abort('Cron Job Not Created');
        }
    }

    /**
     *
     */
    public function execute()
    {
        $email = new Email('default');
        $email->from(['no-replay@house.com' => 'Testing'])
            ->to('loro.vasconcelos@gmail.com')
            ->subject('Teste')
            ->emailFormat('html')
            ->template('markup2')
            ->send();

        debug($this);die;
        $this->SendEmail->main();
    }
}