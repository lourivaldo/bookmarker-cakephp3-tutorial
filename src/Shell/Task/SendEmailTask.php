<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 31/03/2016
 * Time: 21:14
 */

namespace App\Shell\Task;

use Cake\Console\Shell;
use Cake\Mailer\Email;

class SendEmailTask extends Shell
{
    /*public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
    }*/

    public function main()
    {
        $this->out('Task!!');

//        $users = $this->Users->find()->first();
//        $this->out(print_r($users, true));

        $email = new Email('default');
        $email->from(['no-replay@house.com' => 'Testing'])
            ->to('loro.vasconcelos@gmail.com')
            ->subject('About')
            ->template('markup-2')
            ->send();
    }


}