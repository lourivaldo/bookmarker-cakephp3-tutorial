<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 20/03/2016
 * Time: 14:36
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\I18n\Time;

class DataTableComponent extends Component
{
    protected $draw;
    protected $columns;
    protected $order;
    protected $start;
    protected $length;
    protected $search;
    protected $_;
    protected $filters;

    protected $controller;

    public function initialize(array $config)
    {
        $this->controller = $this->_registry->getController();
    }

    public function startup(Event $event)
    {
//        debug($this->request);die;
        if ($this->request->isAjax() && $this->request->query) {
            $this->draw = $this->request->query['draw'];
            $this->columns = $this->request->query['columns'];
            $this->order = $this->request->query['order'];
            $this->start = $this->request->query['start'];
            $this->length = $this->request->query['length'];
            $this->search = $this->request->query['search'];
            $this->_ = $this->request->query['_'];
            $this->filters = $this->request->query['filters'];


            if (!is_null($this->draw)) {
                $start = isset($this->request->query['start']) ? $this->request->query['start'] : 0;
                $length = isset($this->request->query['length']) ? $this->request->query['length'] : 10;
                $page = $start / $length + 1;

                $this->controller->paginate = [
                    'limit' => $length,
                    'page' => $page
                ];

                $sort = $this->columns[$this->order[0]['column']]['data'];
                $direction = $this->order[0]['dir'];

                $this->request->query['sort'] = $sort;
                $this->request->query['direction'] = $direction;

                unset($this->request->query['draw']);
                unset($this->request->query['columns']);
                unset($this->request->query['start']);
                unset($this->request->query['length']);
                unset($this->request->query['search']);
                unset($this->request->query['order']);
                unset($this->request->query['_']);
            }
        }
    }

    public function beforeRender(Event $event)
    {
        if ($this->request->isAjax() && !is_null($this->draw)) {
            $model = $this->controller->modelClass;

            $query = isset($this->controller->paginate['query']) ? $this->controller->paginate['query'] : $this->controller->loadModel($model)->find();
            if (!empty($this->filters)) {
//                debug($this->filters);
                if (isset($this->filters['interval'])) {
                    foreach ($this->filters['interval'] as $interval) {
                        $field = $interval['field'];
                        $start = $interval['start'];
                        $end = $interval['end'];
                        if (isset($interval['type'])) {
                            switch ($interval['type']) {
                                case "date":
                                    $start = Time::parseDate($interval['start']);;
                                    $end = Time::parseDate($interval['end']);;
                                    break;
                            }
                        }
                        $query->where(function ($exp) use ($field, $start, $end) {
                            return $exp->between($field, $start, $end);
                        });

                    }
                }
            }
//            debug($query);
            $data = $this->controller->paginate($query)->toArray();

            $draw = $this->draw;
            $recordsTotal = $query->count();
            $recordsFiltered = $recordsTotal;

            $this->controller->set(compact(['data', 'recordsFiltered', 'recordsTotal', 'draw']));

        }
    }
}