<?php
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
abstract class AppController extends Controller implements Controllable
{

    use BlameTrait;

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('DataTable');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email'],
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Pages',
                'action' => 'display',
                'home'
            ]
        ]);


    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars)
            && in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * beforeFilter callback
     *
     * @param \Cake\Event\Event $event
     * @return void
     */
    public function beforeFilter(Event $event)
    {
//        debug($this->Auth->user());die;
        //$this->Auth->allow(['login', 'logout']);

        if ($this->request->isAjax()) {
            $this->RequestHandler->renderAs($this, 'json');
        }
    }

    /**
     *
     */
    public function news()
    {
        $usersModel = TableRegistry::get("Users");
        $user = $usersModel->get(1);
        $model = TableRegistry::get($this->modelClass);
//        $session = $this->request->session();

        $data = new \stdClass();
        $data->model = $this->modelClass;
        $data->count = 0;
        $data->icon = Router::url(' / ', true) . 'webroot' . DS . 'files' . DS . 'users' . DS . 'photo' . DS . $user->photo_dir . DS . $user->photo;
        $data->date = new Time();
        $data->date->setTimestamp($this->request->data['date'] / 1000);
//        debug($data->date);        die;

        if ($data->date) {
            $data->count = $model->find()
                ->where(['created >= ' => $data->date])
                ->count();
        }

//        $session->write('Notify' . $this->modelClass . 'date', new Time());
        $this->set(compact(['data']));
        $this->set("_serialize", ['data']);
    }
}
