<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 19/03/2016
 * Time: 19:38
 */

namespace App\Controller;


interface Controllable
{
    public function index();

    public function view($id = null);

    public function add();

    public function edit($id = null);

    public function delete($id = null);
}