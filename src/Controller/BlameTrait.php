<?php

namespace App\Controller;

use App\Event\LoggedInUserListener;
use App\Event\RelatedAuditListener;
use AuditLog\Model\Entity\Audit;

/**
 * Class BlameTrait
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 */
trait BlameTrait {

/**
 * {@inheritDoc}
 */
	public function loadModel($modelClass = null, $type = 'Table') {
		$model = parent::loadModel($modelClass, $type);
		$listener = new LoggedInUserListener($this->Auth);
		$model->eventManager()->on($listener);

//		$listener = new RelatedAuditListener(new Audit());
//		$model->eventManager()->on($listener);

		return $model;
	}

} 