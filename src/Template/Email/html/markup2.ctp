<html>
<head>
</head>
<body>
<div itemscope itemtype="http://schema.org/ParcelDelivery">
    <div itemprop="deliveryAddress" itemscope itemtype="http://schema.org/PostalAddress">
        <meta itemprop="streetAddress" content="24 Willie Mays Plaza"/>
        <meta itemprop="addressLocality" content="San Francisco"/>
        <meta itemprop="addressRegion" content="CA"/>
        <meta itemprop="addressCountry" content="US"/>
        <meta itemprop="postalCode" content="94107"/>
    </div>
    <meta itemprop="expectedArrivalUntil" content="2013-03-12T12:00:00-08:00"/>
    <div itemprop="carrier" itemscope itemtype="http://schema.org/Organization">
        <meta itemprop="name" content="FedEx"/>
    </div>
    <div itemprop="itemShipped" itemscope itemtype="http://schema.org/Product">
        <meta itemprop="name" content="iPod Mini"/>
    </div>
    <div itemprop="partOfOrder" itemscope itemtype="http://schema.org/Order">
        <meta itemprop="orderNumber" content="176057"/>
        <div itemprop="merchant" itemscope itemtype="http://schema.org/Organization">
            <meta itemprop="name" content="Bob Dole"/>
        </div>
    </div>
    <link itemprop="trackingUrl" href="http://fedex.com/track/1234567890"/>
    <div itemprop="potentialAction" itemscope itemtype="http://schema.org/TrackAction">
        <link itemprop="target" href="http://fedex.com/track/1234567890"/>
    </div>
</div>
<p>
    This a test for a Go-To action in Gmail.
</p>
</body>
</html>