<div class="row">
    <?= $this->element("Filter/users") ?>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <section class="content">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Criado</th>
                            <th>Modificado</th>
                            <th>Nº Favoritos</th>
                            <th width="80" style="text-align:center;">Ações</th>
                        </tr>
                        </thead>
                    </table>
                </section>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->css("/plugins/bootstrap-datepicker/css/datepicker3", ['block' => true]) ?>
<?= $this->Html->script("/plugins/bootstrap-datepicker/js/bootstrap-datepicker", ['block' => true]) ?>
<?= $this->Html->script("/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR", ['block' => true]) ?>
<?= $this->Html->script("users/index", ['block' => true]) ?>
