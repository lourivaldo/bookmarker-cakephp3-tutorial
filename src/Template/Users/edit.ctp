<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li>
            <?= $this->Form->postLink(__('Delete'), [
                'action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            ) ?>
        </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->input('email');
        echo $this->Form->password('password');
//        echo $this->Form->input('photo', ['type' => 'file']);
        echo $this->Html->image('/files/users/photo/' . $user->photo_dir . '/' . $user->photo);

        echo $this->Form->input('bookmarks.0.id');
        echo $this->Form->input('bookmarks.0.title');
        echo $this->Form->input('bookmarks.0.description');
        echo $this->Form->input('bookmarks.0.url');
        echo $this->Form->input('bookmarks.0.tags.0.title');

        echo $this->Form->input('bookmarks.1.id');
        echo $this->Form->input('bookmarks.1.title');
        echo $this->Form->input('bookmarks.1.description');
        echo $this->Form->input('bookmarks.1.url');
        echo $this->Form->input('bookmarks.1.tags.0.title');
        ?>

    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>

    <?= $this->Form->end() ?>
</div>
