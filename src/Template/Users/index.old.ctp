<div class="row">
    <div class="col-md-12">
        <div class="panel panel-white">
            <div class="panel-body">
                <section class="content">
                    <table class="table table-condensed">
                        <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id', '#') ?></th>
                            <th><?= $this->Paginator->sort('name', 'Nome') ?></th>
                            <th><?= $this->Paginator->sort('email', 'Email') ?></th>
                            <th><?= $this->Paginator->sort('created', 'Criado') ?></th>
                            <th><?= $this->Paginator->sort('modified', 'Modificado') ?></th>
                            <th><?= $this->Paginator->sort('bookmarks_count', 'Nº Favoritos') ?></th>
                            <th width="80" style="text-align:center;">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?= $user->id ?></td>
                                <td><?= h($user->name) ?></td>
                                <td><?= h($user->email) ?></td>
                                <td><?= h($user->created) ?></td>
                                <td><?= h($user->modified) ?></td>
                                <td><?= $user->bookmarks_count ?></td>
                                <td>
                                    <a href="users/edit/<?= $user->id ?>" class="btn btn-xs btn-default">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <h4>Query: <?= $endTime - $startTime . " s" ?></h4>
        <h4>Total: <?= microtime(true) - $startTime . " s" ?></h4>
    </div>
</div>
