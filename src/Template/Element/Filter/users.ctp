<?= $this->Form->input("dateStart", ["label" => "Start", "class" => "date-picker", "value" => "27/03/2016"]) ?>
<?= $this->Form->input("dateEnd", ["label" => "End", "class" => "date-picker", "value" => "02/04/2016"]) ?>
<?= $this->Form->input("booksMin", ["label" => "B Min", "type" => "number", "value" => 3]) ?>
<?= $this->Form->input("booksMax", ["label" => "B Max", "type" => "number", "value" => 5]) ?>
