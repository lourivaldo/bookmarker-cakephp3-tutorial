<!DOCTYPE html>
<html ng-app="bookmarkApp">
<head>
    <title></title>
    <meta charset="UTF-8">
    <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') ?>
    <?= $this->Html->css('https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css') ?>
    <?= $this->Html->css('/plugins/toastr/toastr.min') ?>
    <?= $this->fetch('css') ?>
    <?= $this->Html->scriptBlock('var CONTROLLER = "' . strtolower($this->request->controller) . '";') ?>
    <?= $this->Html->scriptBlock('var ACTION = "' . $this->request->action . '";') ?>
    <?= $this->Html->scriptBlock('var BASE = "' . $this->Url->build('/') . '";') ?>
</head>

<body>

<div class="container" >
    <?= $this->fetch('content') ?>
</div>

<!-- JavaScript assets -->
<?= $this->Html->script('https://code.jquery.com/jquery-1.12.0.min.js') ?>
<?= $this->Html->script('https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js') ?>
<?= $this->Html->script('https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js') ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/visibility.js/1.2.3/visibility.min.js') ?>
<?= $this->Html->script('/plugins/toastr/toastr.min') ?>
<?= $this->Html->script('/plugins/notifyjs/notify') ?>
<!--<?= $this->Html->script('notify') ?>-->
<?= $this->Html->script('/plugins/angular/angular.js') ?>
<?= $this->Html->script('/plugins/angular/angular-animate.js') ?>
<?= $this->Html->script('/plugins/angular/ngDialog/ngDialog.js') ?>
<?= $this->Html->script('/plugins/ui-bootstrap-2.0.2.js') ?>
<?= $this->Html->script('/plugins/angular/angular-messages.min.js') ?>
<?= $this->Html->script('/plugins/angular/i18n/angular-locale_pt-br.js') ?>
<?= $this->Html->script('angular/app.js') ?>
<?= $this->Html->script('angular/controllers/Audit/AuditCtrl.js') ?>
<?= $this->Html->script('angular/services/auditService.js') ?>
<?= $this->fetch('script') ?>

<!--Toast Messages-->
<?= $this->Flash->render() ?>
</body>
</html>