<?php
namespace App\Model\Table;

use Cake\Controller\Component\AuthComponent;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Query;

/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 01/02/2016
 * Time: 22:41
 */
class UsersTable extends AppTable
{

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('users');
        $this->primaryKey('id');

        $this->hasMany("Bookmarks");

        $this->hasMany('Audits', [
            'className' => 'AuditLog.Audits',
            'foreignKey' => 'entity_id',
            'conditions' => ['Audits.model' => 'Users'],
        ]);

        $this->addBehavior('Timestamp');
//        $this->addBehavior('AuditLog', [
//            'user' => 'name',
//            'table' => 'logs',
//            'actions' => ['add', 'edit', 'delete'],
//            'fields' => [
//                'email',
//                'name',
//                'password',
//            ]
//        ]);
        $this->addBehavior('AuditLog.Auditable', [
//            'id' => 'id',
//            'ignore' => ['password'],
            'hahm' => ['Bookmarks'],
            'json_object' => false
        ]);
        $this->addBehavior('Proffer.Proffer', [
            'photo' => [
                'root' => WWW_ROOT . 'files',
                'dir' => 'photo_dir',
                /*'thumbnailSizes' => [
                    'square' => ['w' => 200, 'h' => 200],
                    'portrait' => ['w' => 100, 'h' => 300, 'crop' => true],
                ],
                'thumbnailMethod' => 'imagick'*/
            ]
        ]);
    }


    public function validationHardened(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator->add('password', 'length', ['rule' => ['lengthBetween', 3, 255]]);
        return $validator;
    }

    public function validationDefault(Validator $validator)
    {
//        $validator->notEmpty('name');
//        $validator->notEmpty('password');
//        $validator->add('email', 'valid-email', ['rule' => 'email']);

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {

//        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public function findNewBookmarks(Query $query, array $options)
    {
        $lastDate = $options['lastDate'];
        return $query->where([
            'Bookmarks.created >=' => $lastDate
        ]);
    }
}