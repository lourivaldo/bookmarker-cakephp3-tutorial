<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use SoftDelete\Model\Table\SoftDeleteTrait;

/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 01/02/2016
 * Time: 22:37
 */
class AppTable extends Table
{
    use SoftDeleteTrait;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');
    }
}