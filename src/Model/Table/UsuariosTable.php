<?php
namespace App\Model\Table;

use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Query;
use Cake\ORM\Table;

/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 01/02/2016
 * Time: 22:41
 */
class UsuariosTable extends Table
{

    public static function defaultConnectionName() {
        return 'old';
    }

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');

        $this->table('users');
        $this->primaryKey('id');
    }


    public function validationHardened(Validator $validator)
    {
        $validator = $this->validationDefault($validator);

        $validator->add('password', 'length', ['rule' => ['lengthBetween', 3, 255]]);
        return $validator;
    }

    public function validationDefault(Validator $validator)
    {
        $validator->notEmpty('name');
        $validator->notEmpty('password');
        $validator->add('email', 'valid-email', ['rule' => 'email']);

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

}