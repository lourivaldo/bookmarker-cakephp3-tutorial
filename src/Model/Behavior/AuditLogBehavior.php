<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 04/08/2016
 * Time: 21:15
 */
namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class AuditLogBehavior extends Behavior
{
    protected $_defaultConfig = [
        'id' => 'id',
        'whiteList' => null,
        'blackList' => null,
        'table' => 'logs',
//        'type' => 'logs',
        'user' => 'id',
        'model' => 'model',
        'foreignKey' => 'id',
        'field' => 'field',
        'value' => 'value',
    ];

    private $logs = [];
    private $table;

    public function initialize(array $config)
    {
        $this->table = TableRegistry::get($config['table']);

        parent::initialize($config);
    }

    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        $config = $this->config();

        $foreignKey = $config['foreignKey'];

        if (empty($options['loggedInUser'])) {
            return;
        }

        if ($entity->isNew()) {
            $log = new Entity();
            $log->model = $entity->source();
            $log->type = 'add';
            $this->logs[] = $log;
        } else {
            if (!empty($config['fields'])) {
                foreach ($config['fields'] as $field) {
                    if ($entity->dirty($field)) {
                        $log = new Entity();
//                    $log->foreign_key = empty($entity->id) ? 0 : $entity->$foreignKey;
                        $log->model = $entity->source();
                        $log->field = $field;
                        $log->type = 'update';
                        $log->value = $entity->$field;

                        $this->logs[] = $log;
                    }
                }
            }
        }

    }

    public function afterSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        foreach ($this->logs as $log) {
            $log->foreign_key = $entity->id;
            if (!$this->table->save($log)) {
                Log::error("Não Foi Possível Salvar Log!");
            }
        }
    }

}