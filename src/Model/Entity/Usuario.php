<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 01/02/2016
 * Time: 22:42
 */
class Usuario extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($value)
    {
//        $hash = null;
//        if (!empty($value)) {
            $hasher = new DefaultPasswordHasher();
            $hash = $hasher->hash($value);
//        } else {
//            $hash = $this->_original['password'];
//        }

        return $hash;
    }
}