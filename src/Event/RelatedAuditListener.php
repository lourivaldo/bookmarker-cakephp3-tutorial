<?php
namespace App\Event;

use ArrayObject;
use AuditLog\Model\Entity\Audit;
use Cake\Controller\Component\AuthComponent;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\Entity;

/**
 * Class RelatedAuditListener
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 */
class RelatedAuditListener implements EventListenerInterface {

/**
 * @var AuthComponent
 */
	protected $_audit;

/**
 * Constructor
 *
 * @param \Cake\Controller\Component\AuthComponent $Auth Authcomponent
 */
	public function __construct($_audit) {
		$this->_audit = $_audit;
	}

/**
 * {@inheritDoc}
 */
	public function implementedEvents() {
		return [
			'Model.afterSave' => [
				'callable' => 'afterSave',
				'priority' => -100
			]
		];
	}

/**
 * Before save listener.
 *
 * @param \Cake\Event\Event $event The beforeSave event that was fired
 * @param \Cake\ORM\Entity $entity The entity that is going to be saved
 * @param \ArrayObject $options the options passed to the save method
 * @return void
 */
	public function afterSave(Event $event, Entity $entity, ArrayObject $options) {
//		if (empty($options['child_audits'])) {
			$options['child_audits'][] = $this->_audit;
//		}
	}

}
