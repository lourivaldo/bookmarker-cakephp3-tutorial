<?php
use Cake\Routing\Router;

Router::plugin('AuditLog', [ 'path' => '/audit'],
    function ($routes) {
//        $routes->prefix('admin', function ($routes) {
//            $routes->fallbacks('DashedRoute');
//        });

        $routes->connect('/:prefix/:plugin/:controller/:action', [], ['routeClass' => 'Cake\Routing\Route\InflectedRoute']);

        $routes->fallbacks('DashedRoute');
    });

//bin/cake migrations migrate -p AuditLog