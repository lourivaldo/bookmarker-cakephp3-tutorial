<?php

namespace AuditLog\Controller;


class AuditDeltasController extends AppController
{

    public function index()
    {
        $auditDeltas = $this->AuditDeltas->find()->contain(['Audits.Users']);

        $this->set('auditDeltas', $this->paginate($auditDeltas));
        $this->set(compact(['auditDeltas']));
    }

    public function view($id = null)
    {
        $auditDelta = $this->AuditDeltas->get($id);

        $this->set('auditDelta', $auditDelta);
        $this->set(compact(['auditDelta']));
    }

    public function add()
    {
        // TODO: Implement add() method.
    }

    public function edit($id = null)
    {
        // TODO: Implement edit() method.
    }

    public function delete($id = null)
    {
        // TODO: Implement delete() method.
    }
}
