<?php

namespace AuditLog\Controller;


use Cake\Collection\Collection;

class AuditsController extends AppController
{

    public function index()
    {

        $audits = $this->Audits->find();


        if ($finder = $this->request->query('finder')) {
            try {
                if (is_array($finder)) {
                    foreach ($finder as $f) {
                        $audits->find($f, $this->request->query);
                    }
                } elseif (is_string($finder)) {
                    $audits->find($finder, $this->request->query);
                }
            } catch (\Exception $e) {

            }
        }

        $this->set('audits', $this->paginate($audits));
        $this->set(compact(['audits']));
    }

    public function view($id = null)
    {
        $audit = $this->Audits->get($id, [
            'contain' => ['AuditDeltas', 'RelatedAudits']
        ]);

        $this->set('audit', $audit);
        $this->set(compact(['audit']));
    }

    public function add()
    {
        // TODO: Implement add() method.
    }

    public function edit($id = null)
    {
        // TODO: Implement edit() method.
    }

    public function delete($id = null)
    {
        // TODO: Implement delete() method.
    }
}
