<?php
namespace AuditLog\Model\Table;

use AuditLog\Model\Entity\Audit;
use Cake\Collection\Collection;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Audits Model
 */
class AuditsTable extends Table
{
    public $filterArgs = [];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('audits');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->hasMany('AuditDeltas', [
            'foreignKey' => 'audit_id',
            'className' => 'AuditLog.AuditDeltas'
        ]);

        $this->hasMany('RelatedAudits', [
            'foreignKey' => 'audit_id',
            'className' => 'AuditLog.Audits'
        ]);

        $this->belongsTo("Users", [
            'foreignKey' => 'source_id',
        ]);
        $this->setupSearchPlugin();
    }


    /**
     * Enable search plugin
     *
     * @return null
     */
    public function setupSearchPlugin()
    {
        $this->filterArgs = [
            'event' => [
                'type' => 'value'
            ],
            'model' => [
                'type' => 'value'
            ],
            'source_id' => [
                'type' => 'value'
            ],
            'entity_id' => [
                'type' => 'value'
            ],
        ];

        $this->addBehavior('Search.Search');
    }

    public function findUnusualIps($query, $options)
    {
        $q = $this->find();

        $ips = new Collection($q
            ->select(['ip' => 'source_ip', 'count' => $q->func()->count('source_ip')])
            ->group(['source_ip'])
            ->hydrate(false)
            ->toArray());

        $total = $ips->sumOf('count');
        $unusualIps = $ips->reject(function ($item, $key) use ($total) {
            $c = (int)$item['count'];
            return ($c / $total) > 0.80;
        })->extract('ip');

        return $query->where(['source_ip IN' => $unusualIps->toArray()]);
    }

    public function findDateCreated($query, $options)
    {

    }

    public function findFromEntity($query, $options)
    {
        if (!empty($options['id']) && !empty($options['model']) && !empty($options['relations'])) {
            $id = $options['id'];
            $model = $options['model'];
            $relations = $options['relations'];

            $Table = TableRegistry::get($model);

            $query->where(['model' => $model])->where(['entity_id' => $id]);

            $this->ttt($query, $id, $model, $relations);

        }

        return $query;
    }

    private function ttt($query, $id, $model, $relations)
    {

        if (!count($relations)) {
            return $query;
        }
        $Table = TableRegistry::get($model);
        foreach ($relations as $relation) {
            if (is_array($relation)) {
                $this->ttt($query, $id, $model, $relation);
            }
            $sub = $Table->{$relation}->find()->select('id')->contain([$model])->where([$model . '.' . 'id' => $id]);
            $query = $query->orWhere(function ($exp, $q) use ($relation, $sub) {
                return $exp->eq('model', $relation)->in('entity_id', $sub);
            });
        }
    }

    public function afterSave($event, $entity, $options)
    {
        Log::debug("Audit");
//        Log::debug($options);
    }


}
