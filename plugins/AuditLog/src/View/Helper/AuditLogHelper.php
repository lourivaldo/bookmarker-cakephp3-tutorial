<?php
namespace AuditLog\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * AuditLog helper
 */
class AuditLogHelper extends Helper
{
    public $helpers = ['Text', 'Html'];

    public function getEvent($item)
    {
        switch (strtolower($item->event)) {
            case 'create'    :
                return '<span class="label label-success">' . __('create') . '</span>';
            case 'edit'        :
                return '<span class="label label-info">' . __('edit') . '</span>';
            case 'delete'    :
                return '<span class="label label-danger">' . __('delete') . '</span>';
            default            :
                return '<span class="label label-default">' . __($item->event) . '</span>';
        }
    }

    public function getEntityLink($item, $action = "edit", $plugin = false)
    {
        $arr = explode(".", $item->model);
        $model = end($arr);

        if (count($arr) > 1) {
            $plugin = $arr[0];
        }

        return $this->Html->link($item->entity_id, [
            'plugin' => $plugin,
            'controller' => $model,
            'action' => $action,
            $item->entity_id]);
    }

    public function getSourceLink($item, $action = "edit", $plugin = false)
    {
        $arr = explode(".", $item->source_model);
        $model = end($arr);

        if (count($arr) > 1) {
            $plugin = $arr[0];
        }

        return $this->Html->link($item->source_id, [
            'plugin' => $plugin,
            'controller' => $model,
            'action' => $action,
            $item->source_id]);
    }

    public function getSourceId($item)
    {
        return $item->source_id;
    }

    public function getSource($item)
    {
        return $item->source_id;
    }

    public function outputHtmlValue($value)
    {
        if (is_array($value)) {
            $output = '<dl class="dl-horizontal">';
            foreach ($value as $key => $val) {
                $output .= sprintf(
                    '<dt>%s<dt/><dd>%s</dd>',
                    $key,
                    $this->outputValue($val)
                );
            }
            return $output . '</dl>';
        }
        return sprintf(
            '<span title="%s">%s</span>',
            $value,
            $this->outputValue($value)
        );
    }

    public function outputValue($value)
    {
        if (is_array($value)) {
            $output = '<dl class="dl-horizontal">';
            foreach ($value as $key => $val) {
                $output .= sprintf(
                    '<dt>%s<dt/><dd>%s</dd>',
                    $key,
                    $this->outputValue($val)
                );
            }
            return $output . '</dl>';
        }
        if ($value === NULL) {
            return "(" . __('NULL') . ")";
        }

        if ($value === '') {
            return "(" . __('EMPTY') . ")";
        }

        return h($this->Text->truncate($value, 50));
    }

    public function getIdentifier($item)
    {
        if (empty($this->_View->viewVars['model'])) {
            $name = $item['entity_id'];
        } else {
            $model = $this->_View->viewVars['model'];
            $displayField = $this->_View->viewVars['displayField'];

            $name = $item[$model][$displayField];
        }

        return $this->Text->truncate($name, 50);
    }

    public function getDiff($field, $new, $old)
    {
        $config = [
            'ignoreNewLines' => true,
            'ignoreWhitespace' => true,
            'ignoreCase' => true
        ];

        $diff = new \Diff((array)$new, (array)$old, $config);
        return $diff->render(new DiffRendererHtmlSideBySide());
    }

}
