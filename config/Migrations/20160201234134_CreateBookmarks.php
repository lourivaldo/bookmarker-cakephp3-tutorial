<?php

use config\Migrations\AppMigration;

class CreateBookmarks extends AppMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bookmarks');
        $table->addColumn('user_id', 'integer');
        $table->addColumn('title', 'string');
        $table->addColumn('description', 'text', [
            'null' => true
        ]);
        $table->addColumn('url', 'text');
        $this->timestamps($table);
        $this->softDeletes($table);
        $table->addForeignKey('user_id', 'users', 'id');
        $table->create();
    }
}
