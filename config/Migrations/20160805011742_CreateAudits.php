<?php
use Migrations\AbstractMigration;

class CreateAuditsS extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('audits');
        $table->addColumn('foreign_key', 'integer');
        $table->addColumn('model', 'string');
        $table->addColumn('entity_id', 'string');

//        $table->addColumn('field', 'string');

        $table->addColumn('old_value', 'text');
        $table->addColumn('new_value', 'text');

        $table->addColumn('url', 'string');
        $table->addColumn('ip', 'string');

        $table->addColumn('type', 'string', [
            'default' => 'update'
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
        ]);
        $table->create();
    }
}
