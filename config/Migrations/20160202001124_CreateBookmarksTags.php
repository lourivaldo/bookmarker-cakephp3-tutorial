<?php

use config\Migrations\AppMigration;

class CreateBookmarksTags extends AppMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('bookmarks_tags');
        $table->addColumn('bookmark_id', 'integer');
        $table->addColumn('tag_id', 'integer');
        $this->timestamps($table);
        $this->softDeletes($table);
        $table->addForeignKey('bookmark_id', 'bookmarks', 'id');
        $table->addForeignKey('tag_id', 'tags', 'id');
        $table->addPrimaryKey(['bookmark_id', 'tag_id']);
        $table->create();
    }
}
