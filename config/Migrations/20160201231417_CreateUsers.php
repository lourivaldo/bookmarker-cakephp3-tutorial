<?php

use config\Migrations\AppMigration;

class CreateUsers extends AppMigration
{
    /**
     * bin/cake bake migration CreateUsers email:string:unique:EMAIL_INDEX password:string created modified deleted
     *
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('name', 'string');
        $table->addColumn('email', 'string');
        $table->addColumn('password', 'string');
        $table->addColumn('bookmarks_count', 'integer', ['default' => 0]);
        $this->addTimestamps($table);
        $this->softDeletes($table);

        $table->addIndex(['email'], [
            'unique' => true,
        ]);
        $table->create();
    }
}
