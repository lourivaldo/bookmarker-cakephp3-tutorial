<?php

use config\Migrations\AppMigration;

class CreateTags extends AppMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tags');
        $table->addColumn('title', 'string');
        $this->timestamps($table);
        $this->softDeletes($table);
        $table->addIndex('title', [
            'unique' => true
        ]);
        $table->create();
    }
}
