<?php
use Migrations\AbstractMigration;

class AddPhotoAndPhotoDirToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('photo', 'string', ['null' => true]);
        $table->addColumn('photo_dir', 'string', ['null' => true]);
        $table->update();
    }
}
