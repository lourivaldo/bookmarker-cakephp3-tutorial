<?php

namespace config\Migrations;

use Migrations\AbstractMigration;

/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 13/03/2016
 * Time: 13:09
 */
abstract class AppMigration extends AbstractMigration
{
    public function timestamps($table)
    {
        $table->addColumn('created', 'datetime', [
            'default' => 'CURRENT_TIMESTAMP',
        ]);
        $table->addColumn('modified', 'datetime', [
            'null' => true,
        ]);
    }

    public function softDeletes($table)
    {
        $table->addColumn('deleted', 'datetime', [
            'null' => true,
        ]);
    }
}