<?php
use Cake\Auth\DefaultPasswordHasher;
use Phinx\Seed\AbstractSeed;
use Cake\ORM\TableRegistry;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{

    public function init()
    {
        $this->Users = TableRegistry::get('users');
        $this->Usuarios = TableRegistry::get('usuarios');
    }

    /**
     * To execute
     *      bin/cake migrations seed
     * To creates
     *      bin/cake bake seed Users
     *
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {

        $hasher = new DefaultPasswordHasher();
        $data = [
            'name' => 'lourivaldo',
            'email' => 'lourivaldo@gmail.com',
            'password' => $hasher->hash('1234')
        ];


        $users = $this->table('users');
        /*$users->insert($data)->save();*/


        debug($this->Users->find()->order(['id' => 'DESC'])->first());
        debug($this->Usuarios->find()->order(['id' => 'DESC'])->first());
//        $user = $this->Users->newEntity($data);
//        debug($this->Users->save($user));
    }
}
