<?php
use Phinx\Seed\AbstractSeed;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Providers seed.
 */
class FidelitiesSeed extends AbstractSeed
{

    /**
     * public static function defaultConnectionName() {
    return 'old';
    }
     */
    public function init()
    {
        $this->Fidelities = TableRegistry::get('Fidelities');
        $this->Providers = TableRegistry::get('Providers');
        $this->Fornecedores = TableRegistry::get('Fornecedores');
    }

    public function run()
    {
        $fornecedoresIds = $this->Fornecedores->find()->select(['id']);
        $time = new Time();

        foreach ($fornecedoresIds as $f) {
            try {
                $fornecedor = $this->Fornecedores->get($f->id);
                $provider = $this->Providers->findByCpfOrEmail($fornecedor->cpf, $fornecedor->email)->first();

                if ($provider) {
                    $fidelities = [];
                    if (!empty($fornecedor['cartao_fidelidade_tam'])) {
                        $fidelities[] = [
                            'card_number' => $fornecedor['cartao_fidelidade_tam'],
                            'access_password' => $fornecedor['senha_multiplus'],
                            'digital_signature' => $fornecedor['assinatura_eletronica_tam'],
                            'validity' => $fornecedor['data_vencimento_tam'],
                            'provider_id' => $provider->id,
                            'program_id' => 1,
                            'created' => $time,
                            'modified' => $time,
                            'created_by' => 1
                        ];
                    }

                    if (!empty($fornecedor['numero_amigo_avianca'])) {
                        $fidelities[] = [
                            'card_number' => $fornecedor['numero_amigo_avianca'],
                            'access_password' => $fornecedor['senha_avianca'],
                            'validity' => $fornecedor['data_vencimento_avianca'],
                            'program_id' => 4,
                            'created' => $time,
                            'modified' => $time,
                            'created_by' => 1,
                            'provider_id' => $provider->id
                        ];
                    }

                    if (!empty($fornecedor['numero_cartao_azul'])) {
                        $fidelities[] = [
                            'card_number' => $fornecedor['numero_cartao_azul'],
                            'access_password' => $fornecedor['senha_resgate_azul'],
                            'validity' => $fornecedor['data_vencimento_azul'],
                            'program_id' => 3,
                            'created' => $time,
                            'modified' => $time,
                            'created_by' => 1,
                            'provider_id' => $provider->id,
                        ];
                    }

                    if (!empty($fornecedor['numero_smiles_gol'])) {
                        $fidelities[] = [
                            'card_number' => $fornecedor['numero_smiles_gol'],
                            'access_password' => $fornecedor['senha_gol'],
                            'validity' => $fornecedor['data_vencimento_gol'],
                            'program_id' => 2,
                            'created' => $time,
                            'modified' => $time,
                            'created_by' => 1,
                            'provider_id' => $provider->id,
                        ];
                    }

                    foreach ($fidelities as $fidelity) {
                        $entity = $this->Fidelities->newEntity($fidelity);
                        $save = $this->Fidelities->save($entity);

                        if ($save) {
                            echo "F Salvo " . $save->id;
                        } else {
                            echo "F Nao Salvo ";
                        }
                    }

                    echo "Migrado fornecedor " . $fornecedor->id;
                    die;
                } else {
                    debug("Fornecedor nao existe " . $fornecedor->id);
                }
            } catch (Exception $e) {
                debug($e->getMessage());
            }
        }


    }
}