<?php
use Phinx\Seed\AbstractSeed;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * Providers seed.
 */
class ProvidersFilesSeed extends AbstractSeed
{

    /**
     * public static function defaultConnectionName() {
     * return 'old';
     * }
     */
    public function init()
    {
        $this->ProvidersFiles = TableRegistry::get('ProvidersFiles');
        $this->Providers = TableRegistry::get('Providers');

        $this->Fornecedores = TableRegistry::get('Fornecedores');
        $this->FornecedoresFiles = TableRegistry::get('FornecedoresFiles');
    }

    public function run()
    {
        $fornecedoresIds = $this->Fornecedores->find()->select(['id']);
        $time = new Time();

        foreach ($fornecedoresIds as $f) {
            try {
                $fornecedor = $this->Fornecedores->get($f->id);
                $provider = $this->Providers->findByCpfOrEmail($fornecedor->cpf, $fornecedor->email)->first();

                if ($provider) {

                    $fornecedoresFiles = null;

                    foreach ($fornecedoresFiles as $file) {
                        $entity = $this->ProvidersFiles->newEntity();
                        $entity->file = null;
                        $entity->dir = null;

                        $save = $this->ProvidersFiles->save($entity);

                        if ($save) {
                            echo "F Salvo " . $save->id;
                        } else {
                            echo "F Nao Salvo ";
                        }
                    }

                    echo "Migrado fornecedor " . $fornecedor->id;
                    die;
                } else {
                    debug("Fornecedor nao existe " . $fornecedor->id);
                }
            } catch (Exception $e) {
                debug($e->getMessage());
            }
        }


    }
}